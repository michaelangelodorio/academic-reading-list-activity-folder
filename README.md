# 230825/230828 - Batch 297:  Academic Reading List 

### **Topics**

- React JS Resources
  - [Create a New React App](https://reactjs.org/docs/create-a-new-react-app.html)
  - [Virtual DOM](https://www.youtube.com/watch?v=M-Aw4p0pWwg)
  - [Introducing JSX](https://reactjs.org/docs/introducing-jsx.html)
  - [JSX Fragment](https://legacy.reactjs.org/docs/fragments.html#short-syntax)
  - [React-Bootstrap Documentation](https://react-bootstrap.github.io/)
  - [React JS Components and Props](https://reactjs.org/docs/components-and-props.html)
- React JS Props
  - [resources](https://reactjs.org/docs/components-and-props.html)
- Props Drilling
  - [resources](https://medium.com/front-end-weekly/props-drilling-in-react-js-723be80a08e5)
- Lists and Keys
  - [resources](https://reactjs.org/docs/lists-and-keys.html)
- Typechecking With PropTypes
  - [resources](https://reactjs.org/docs/typechecking-with-proptypes.html)
- React JS States
  - [resources](https://reactjs.org/docs/hooks-state.html;)

Advance Reading:
- React JS useEffect Hook
  - [resources](https://reactjs.org/docs/hooks-effect.html#gatsby-focus-wrapper)
- React-Bootstrap Form Component
  - [resources](https://react-bootstrap.github.io/components/forms/)
- React JS Events
  - [resources](https://reactjs.org/docs/events.html)


### **Purpose**
- Review and practice creating a React project, creating components, add React States, add props passing to those components.
- Create new react application from scratch with create-react-app with react and react-bootstrap for styling.
- Practice on creating states in React and pass data between parent and child components using props by following the steps in making React projects such as Counter App and Todo List App

### Activity 1: Counter App

### Activity 2: Todo List Application
