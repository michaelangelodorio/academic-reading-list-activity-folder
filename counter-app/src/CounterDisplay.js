import React from 'react';

function CounterDisplay({count}){
    return (
       <h5 className='card-title text-warning-emphasis'>Count: {count}</h5>
    )
}

export default CounterDisplay;