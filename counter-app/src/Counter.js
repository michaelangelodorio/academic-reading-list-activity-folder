import React, { useState } from 'react';
import CounterDisplay from './CounterDisplay';
import { Button, Card} from 'react-bootstrap';

function Counter(){
const [count, setCount] = useState(0);

function increment(){
    setCount(count + 1);
}
function decrement(){
    if (count > 0) {
        setCount(count - 1);     
    }
    else{
        return;
    } 
}

console.log('Count: ' + count)

return (
    <div className='m-1 py-2'>
        <Card className='card-style p-1 bg-warning-subtle '>
        <Card.Body>
            <Card.Header className='text-warning-emphasis text-center h3'>Counter</Card.Header>
            <Card.Text>This will modify the count state variable when the corresponding buttons are clicked. </Card.Text>
            <Card.Title> <CounterDisplay count={count} /></Card.Title>
            <div className='text-center'>
                <Button className='p-2 m-2' variant="danger" onClick={decrement}>decrement</Button>
                <Button className='p-2 m-2' variant="success" onClick={increment}>increment</Button>
            </div>
        </Card.Body>   
        </Card>
    </div>
    )
}

export default Counter;