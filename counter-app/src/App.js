import React from 'react';
import Counter from './Counter';
import './App.css';

function App(){
  return (
      <div className="card-style mx-auto p-2">
        <Counter />
      </div>
  );
}

export default App;