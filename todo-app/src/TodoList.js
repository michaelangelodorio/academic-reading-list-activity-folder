import React from 'react';
import TodoItem from './TodoItem';

function TodoList({todos, onDelete}) {
  return (
      <div>
          {
              // Using the map function to iterate through each 'todo' in the 'todos' array
              todos.map((todo) => (
                  // Rendering the TodoItem component for each 'todo' in the array
                  <TodoItem
                      key={todo.id}      // Assigning a unique key to each TodoItem for efficient rendering
                      todo={todo}        // Passing the 'todo' object as a prop to the TodoItem component
                      date={todo.date}   // Passing the 'date' property of the 'todo' object as a prop
                      onDelete={onDelete} // Passing the 'onDelete' function as a prop to handle delete action
                  />
              ))
          }
      </div>
  );
}

export default TodoList;